---
title: אירועים
events-selected: yes
---

# עקבו אחר האירועים שלנו:

<i class="fa fa-telegram fa-lg" aria-hidden="true"></i>
טלגרם:
[לוח אירועי המרד בהכחדה](https://t.me/joinchat/S5zBIgQIWPKy0Rjq),
[לוח אירועי תרבות מתחדשת](https://t.me/xr_il_regen_events)

<i class="fa fa-email-bulk fa-lg" aria-hidden="true"></i>
דואר אלקטרוני:
[רשימת תפוצה](https://lists.riseup.net/www/info/xr-il-regen-news)

- <i class="fa fa-user-plus" aria-hidden="true"></i>
  הרשמה
  + היכנסו
    ל[קישור](https://lists.riseup.net/www/info/xr-il-regen-news)
    ולחצו **`Subscribe`** מצד שמאל
  + או
    [שלחו הודעה בדוא"ל](mailto:xr-il-regen-news-subscribe@lists.riseup.net)
    (ההודעה יכולה להיות ריקה, זה הליך אוטומטי)
  + או
    [צרו קשר](/contact) ונוסיף אתכןם
- <i class="fa fa-user-times" aria-hidden="true"></i>
  הסרה
  + היכנסו
    ל[קישור](https://lists.riseup.net/www/info/xr-il-regen-news)
    ולחצו **`Unsubscribe`** מצד שמאל
  + או
    [שלחו הודעה בדוא"ל](mailto:xr-il-regen-news-unsubscribe@lists.riseup.net)
    (ההודעה יכולה להיות ריקה, זה הליך אוטומטי)
  + או
    [צרו קשר](/contact) ונסיר אתכןם
- <i class="fa fa-archive" aria-hidden="true"></i>
  [ארכיון](https://lists.riseup.net/www/arc/xr-il-regen-news) ההודעות שנשלחו
  לרשימה עד כה

<i class="fa fa-mastodon fa-lg" aria-hidden="true"></i>
מסטודון:
<span dir="ltr">
[`@xr_il_regen_events@tooot.im`](https://tooot.im/@xr_il_regen_events)
</span>

<i class="fa fa-matrix-org fa-lg" aria-hidden="true"></i>
מטריקס / אלמנט:
<span dir="ltr">
[`#xr-il-regen-events:matrix.org`](https://matrix.to/#/#xr-il-regen-events:matrix.org)
</span>

# האירועים הקרובים:

$partial("templates/event-list.html")$

אפשר לעיין ברשימת
[האירועים האחרונים שהיו](/past).
