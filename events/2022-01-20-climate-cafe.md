---
date: 2022-01-20 20:30:00+0200
image: 2022-01-20-climate-cafe-nirit.png
day: 20
month: ינואר
year: 2022
title: קפה אקלים - סדנה חווייתית בנושא משבר האקלים
register-with: נירית 050-6622791
contact-text: צרו קשר עם
contact-name: נירית 050-6622791
online-app-name: זום
online-app-url: ""
date-day: חמישי, 20 בינואר 2022
date-hour-from: 20:30
date-hour-to: 22:00
org-name: בית לרוח
org-link: https://www.bait-la-ruach.co.il
---

דווקא אנו, כאקטיביסטים פעילים המודעים לחומרת המצב, נוטים מדי פעם לשקוע בעצב
עמוק וחרדה אל מול פני העתיד. אל תישארו עם זה לבד. הצטרפו למפגש מיוחד בהשראת
"העשייה שמחברת מחדש" של הפעילה האקולוגית הותיקה ג׳ואנה מייסי.
