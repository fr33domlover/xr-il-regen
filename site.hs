{- Written in 2021, 2022 by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

{-# LANGUAGE OverloadedStrings #-}

import Data.List
import Data.Monoid -- (mappend)
import Data.Time.Clock
import Data.Time.Format
import Data.Traversable
import Hakyll
import System.FilePath -- ((</>), dropExtension)

config :: Configuration
config = defaultConfiguration
  { deployCommand =
        "rsync \
            \--rsh='ssh -p 5032'   \
            \--recursive           \
            \--chown=hakyll:hakyll \
            \--compress            \
            \--delete              \
            \--verbose             \
            \_site/                \
            \hakyll@fr33domlover.site:xr-il-regen"
  }

pageRoute :: Routes
pageRoute = customRoute $ (</> "index.html") . dropExtension . toFilePath

postCtx :: Context String
postCtx =
    dateField "date" "%B %e, %Y" `mappend`
    defaultContext

main :: IO ()
main = getCurrentTime >>= main'

main' :: UTCTime -> IO ()
main' now = hakyllWith config $ do
    match "fonts/*" $ do
        route   idRoute
        compile copyFileCompiler

    match "images/*" $ do
        route   idRoute
        compile copyFileCompiler

    match "style/*" $ do
        route   idRoute
        compile compressCssCompiler

    match (fromList ["about.md", "contact.md", "offers.md"]) $ do
        route pageRoute
        compile $ pandocCompiler
            >>= loadAndApplyTemplate "templates/default.html" defaultContext
            >>= relativizeUrls

    match "events/*" $ do
        route pageRoute
        compile $ do
            time <- do
                item <- getResourceBody
                getItemUTC defaultTimeLocale $ itemIdentifier item
            let ctx = postCtx <>
                      if time < now then constField "passed" "yes" else mempty
            pandocCompiler
                >>= loadAndApplyTemplate "templates/event.html"   ctx
                >>= loadAndApplyTemplate "templates/default.html" ctx
                >>= relativizeUrls

    match "index.md" $ do
        route $ setExtension "html"
        compile $ do
            eventsWithTime <- do
                items <- loadAll "events/*"
                for items $ \ event -> do
                    time <- getItemUTC defaultTimeLocale $ itemIdentifier event
                    return (time, event)
            let events =
                    map snd $ sortOn fst $
                    filter ((>= now) . fst) eventsWithTime
                ctx = listField "events" postCtx (pure $ take 3 events)
                   <> (if (null events)
                        then constField "no-events" "yes"
                        else mempty
                      )
                   <> defaultContext
            pandocCompiler
                >>= applyAsTemplate ctx
                >>= loadAndApplyTemplate "templates/default.html" ctx
                >>= relativizeUrls








    match "events.md" $ do
        route pageRoute
        compile $ do
            eventsWithTime <- do
                items <- loadAll "events/*"
                for items $ \ event -> do
                    time <- getItemUTC defaultTimeLocale $ itemIdentifier event
                    return (time, event)
            let events =
                    map snd $ sortOn fst $
                    filter ((>= now) . fst) eventsWithTime
                ctx = listField "events" postCtx (pure events)
                   <> (if (null events)
                        then constField "no-events" "yes"
                        else mempty
                      )
                   <> defaultContext
            pandocCompiler
                >>= applyAsTemplate ctx
                >>= loadAndApplyTemplate "templates/default.html" ctx
                >>= relativizeUrls

    match "past.md" $ do
        route pageRoute
        compile $ do
            eventsWithTime <- do
                items <- loadAll "events/*"
                for items $ \ event -> do
                    time <- getItemUTC defaultTimeLocale $ itemIdentifier event
                    return (time, event)
            let events =
                    reverse $ map snd $ sortOn fst $
                    filter ((< now) . fst) eventsWithTime
                eventCtx = postCtx
                        <> constField "passed" "yes"
                ctx = listField "events" eventCtx (pure events)
                   <> defaultContext
            pandocCompiler
                >>= applyAsTemplate ctx
                >>= loadAndApplyTemplate "templates/default.html" ctx
                >>= relativizeUrls

    match "templates/*" $ compile templateBodyCompiler
