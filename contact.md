---
title: יצירת קשר
contact-selected: yes
---

נשמח לדבר! ונשמח שתצטרפו אלינו!

<i class="fa fa-envelope fa-lg" aria-hidden="true"></i>
דואר אלקטרוני:
[xr-il-regen@riseup.net](mailto:xr-il-regen@riseup.net)

<i class="fa fa-telegram fa-lg" aria-hidden="true"></i>
טלגרם:
[המרד בהכחדה](https://t.me/xrisrael)
ובתוכו בין השאר:

- [קבוצת הדיונים הכללית](https://t.me/joinchat/AAAAAErmdx23OD1sL5UKOQ)
- [תמיכה וייעוץ למורד.ת](https://t.me/joinchat/Fk6_bxn67QyZh5N2KB7Tog)
- [הקו החם לתמיכה רגשית](https://t.me/joinchat/G4fRuRCMPQlkv0Xz)
- ואם אתןם חושבות.ים להצטרף אלינו (יאי!), לחצו על קישור ההצטרפות
  [לקבוצת תרבות מתחדשת](https://t.me/+PrTZW2OcNY1lOGQ0)
  ונדבר!

ניתן גם לעקוב אחרי האירועים והפרסומים שלנו במגוון ערוצים!

<a class="pure-button pure-button-primary" href="/events">עקבו אחרינו</a>
