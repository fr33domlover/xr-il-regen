This is the source material for the website of [Extinction Rebellion][XR]
[Israel][XRIL]'s Regenerative Culture group.

The pages are written in Markdown and compilation instructions and written in
[Haskell][] using [Pandoc][] and [Hakyll][]. The style is [PureCSS][].

Released to the public domain using [CC0][]. The CSS files in the 'style'
folder whose name begins with "pure" are from PureCSS and are licensed under
the BSD 3-clause license. The KtavYad font files in the 'fonts' folder are from
the [Culmus][] project by Maxim Iorsh and licensed under [GPL v2][GPL2].  The
Caveat font in the 'fonts' folder is by Pablo Impallari, licensed under
[OFL][]. The [ForkAwesome][] icon font is licensed under [OFL][] as well, and
its CSS is under MIT license.

Build instructions:

    stack build
    stack run build
    stack run watch

Then browse to `127.0.0.1:8000` to see the website in action.

[XR]: https://rebellion.global
[XRIL]: https://xrisrael.earth
[Pandoc]: https://pandoc.org
[Hakyll]: https://jaspervdj.be/hakyll/
[Haskell]: https://haskell.org
[PureCSS]: https://purecss.io
[CC0]: https://creativecommons.org/publicdomain/zero/1.0/
[Culmus]: https://culmus.sourceforge.io/
[GPL2]: http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
[OFL]: http://scripts.sil.org/OFL
[ForkAwesome]: https://forkaweso.me
