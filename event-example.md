---
date: 2021-12-29 18:00:00+0200
image: 2021-12-29-example-event.png
day: 29
month: דצמבר
year: 2021
title: אירוע לדוגמה
register-form: https://registration.link
contact: contact@person.email
location-name: שיח בכפר
location-area: תל אביב
location-name-url: https://villagebush.net
location-address: תמוז 52/3, תל סתיו
location-address-url: https://www.openstreetmap.org/#map=19/31.25818/34.78919
date-day: 29 בדצמבר 2021
date-hour-from: 18:00
date-hour-to: 20:00
org-name: קבוצת תרבות מתחדשת
org-link: https://regen.xrisrael.earth/contact
---

קהל יעד: כולןם

מה בתכנית: ככה וככה וככה

מיקום ושעה: בית הבלה בלה, 32 בחודשצמבר 2025 בשעה 8 בערב

נתראה שם!
